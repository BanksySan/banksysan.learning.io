﻿namespace BanksySan.Learning.Io
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Threading;

    public class ContinuousStream : Stream
    {
        private readonly IProducerConsumerCollection<byte> _buffer;
        private readonly AutoResetEvent _readWait = new AutoResetEvent(false);
        public ContinuousStream() => _buffer = new ConcurrentQueue<byte>();

        public override bool CanRead => true;
        public override bool CanSeek => false;
        public override bool CanWrite => true;
        public override bool CanTimeout => false;
        public bool WritingFinished { get; private set; }

        public void CloseWrite()
        {
            WritingFinished = true;
            _readWait.Set();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var maxByteCount = offset + count > buffer.Length ? buffer.Length : count;

            if (_buffer.Count == 0)
            {
                _readWait.WaitOne();
                
            }
            _readWait.Reset();
            var actualBytesRead = 0;

            for (var i = offset; i < maxByteCount && _buffer.TryTake(out var b); i++)
            {
                buffer[i] = b;
                actualBytesRead++;
            }

            return actualBytesRead;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (WritingFinished)
            {
                throw new InvalidOperationException($"Attempt to write after {nameof(CloseWrite)}() has been called.");
            }

            var maxByteCount = offset + count > buffer.Length ? buffer.Length : count;
            for (var i = offset; i < maxByteCount; i++)
            {
                _buffer.TryAdd(buffer[i]);
            }
            _readWait.Set();
        }

        #region Not imlemented

        public override void Flush()
        {
        }

        public override long Seek(long offset, SeekOrigin origin) =>
            throw new NotSupportedException("Seek not supported");

        public override void SetLength(long value)
        {
            throw new NotSupportedException("SetLength not supported");
        }

        public override long Length => throw new NotSupportedException();

        public override int ReadTimeout
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }

        public override int WriteTimeout
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }

        public override long Position
        {
            get => throw new NotSupportedException();

            set => throw new NotSupportedException();
        }

        #endregion
    }
}