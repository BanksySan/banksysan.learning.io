namespace BanksySan.Learning.Io.Tests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ContinuousStreamTests
    {
        public ContinuousStreamTests(ITestOutputHelper outputHelper) => _outputHelper = outputHelper;

        [SuppressMessage("ReSharper", "NotAccessedField.Local")]
        private readonly ITestOutputHelper _outputHelper;

        [Fact]
        public void CanReadAndWrite()
        {
            var input = new byte[] {3, 2, 1};

            var output = new byte[input.Length];
            using (var stream = new ContinuousStream())
            {
                stream.Write(input, 0, input.Length);

                stream.Read(output, 0, input.Length);
            }

            Assert.Equal(input, output);
        }

        [Fact]
        public void CanSupportMixedReadingAndWriting()
        {
            using (var stream = new ContinuousStream())
            {
                var input1 = new byte[] {9, 7};
                var input2 = new byte[] {5};
                var input3 = new byte[] {3, 1};

                var expectedLength = input1.Length + input2.Length + input3.Length;
                var expectedOutput = new byte[expectedLength];
                Array.Copy(input1, 0, expectedOutput, 0, input1.Length);
                Array.Copy(input2, 0, expectedOutput, input1.Length, input2.Length);
                Array.Copy(input3, 0, expectedOutput, input1.Length + input2.Length, input3.Length);
                var actualOutput = new byte[expectedLength];

                var totalReadCount = 0;
                int bytesRead;

                stream.Write(input1, 0, input1.Length);
                bytesRead = stream.Read(actualOutput, totalReadCount, 5);
                totalReadCount += bytesRead;
                Assert.Equal(bytesRead, input1.Length);


                stream.Write(input2, 0, input2.Length);
                bytesRead = stream.Read(actualOutput, totalReadCount, 5);
                Assert.Equal(bytesRead, input2.Length);
                totalReadCount += bytesRead;

                stream.Write(input3, 0, input3.Length);
                bytesRead = stream.Read(actualOutput, totalReadCount, 5);
                Assert.Equal(bytesRead, input3.Length);

                Assert.Equal(expectedOutput, actualOutput);
            }
        }

        [Fact]
        public void FeatureTestPropertiesAreCorrect()
        {
            var stream = new ContinuousStream();

            Assert.True(stream.CanRead);
            Assert.True(stream.CanWrite);
            Assert.False(stream.CanSeek);
            Assert.False(stream.CanTimeout);
        }

        [Fact]
        public void FlushDoesNotThrowException()
        {
            var stream = new ContinuousStream();
            stream.Flush();
        }

        [Fact]
        public void OnlyReturnZeroWhenStreamIsClosed()
        {
            var stream = new ContinuousStream();
            var readerTask = new Task(() =>
            {
                var readBuffer = new byte[1];

                var readCount = stream.Read(readBuffer, 0, readBuffer.Length);

                while (readCount != 0)
                {
                    readCount = stream.Read(readBuffer, 0, readBuffer.Length);
                }

                Assert.True(stream.WritingFinished, $"{nameof(stream.WritingFinished)} should be true when reading finishes.");
            });

            readerTask.Start();
            Assert.False(stream.WritingFinished);
            stream.WriteByte(1);
            stream.WriteByte(1);
            Thread.Sleep(100);
            stream.CloseWrite();
            readerTask.Wait();

            Assert.True(stream.WritingFinished);
        }

        [Fact]
        public void UnSupportedMethodsThrowCorrectException()
        {
            var stream = new ContinuousStream();

            Assert.Throws<NotSupportedException>(() => stream.Length);
            Assert.Throws<NotSupportedException>(() => stream.Position);
            Assert.Throws<NotSupportedException>(() => stream.SetLength(default(long)));
            Assert.Throws<NotSupportedException>(() => stream.Seek(default(long), default(SeekOrigin)));
            Assert.Throws<NotSupportedException>(() => stream.WriteTimeout = default(int));
            Assert.Throws<NotSupportedException>(() => stream.ReadTimeout = default(int));
            Assert.Throws<NotSupportedException>(() => stream.WriteTimeout);
            Assert.Throws<NotSupportedException>(() => stream.ReadTimeout);
        }

        [Fact]
        public void WritingAfterFinishingThrowsException()
        {
            var stream = new ContinuousStream();

            stream.WriteByte(1);
            stream.CloseWrite();
            var e = Assert.Throws<InvalidOperationException>(() => stream.WriteByte(1));
            Assert.Equal(e.Message, $"Attempt to write after {nameof(stream.CloseWrite)}() has been called.");
        }
    }
}